﻿//#define FIX64_IMPLICIT_TO_ENABLED
#define FIX64_IMPLICIT_FROM_ENABLED
#define FIX64_CHECKED
//#define FIX64_CHECKED_OPERATOR

using Newtonsoft.Json;
using System;
using System.IO;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Tests.C11")]
namespace FixMath.NET
{
    public class Fix64Converter : JsonConverter<Fix64>
    {
        public override Fix64 ReadJson(JsonReader reader, Type objectType, Fix64 existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null && objectType == typeof(Fix64?))
                return 0;
            return Fix64.Convert(reader.Value);
        }

        public override void WriteJson(JsonWriter writer, Fix64 value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToJString());
        }
    }

    public class NullableFix64Converter : JsonConverter<Fix64?>
    {
        public override Fix64? ReadJson(JsonReader reader, Type objectType, Fix64? existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                if (objectType == typeof(Fix64?))//If we're expecting a nullable, use null
                    return null;
                else //If we're not expecting a nullable, use 0.
                    return 0;

            return Fix64.Convert(reader.Value);//Else our value is not null, so we can return the true value.
        }

        public override void WriteJson(JsonWriter writer, Fix64? value, JsonSerializer serializer)
        {
            if (value.HasValue)
                writer.WriteValue(value.Value.ToJString());
            else
                writer.WriteNull();
        }
    }


    /// <summary>
    /// Represents a Q31.32 fixed-point number.
    /// </summary>
    [Serializable]
    [JsonConverter(typeof(NullableFix64Converter))]
    //[JsonConverter(typeof(Fix64Converter))]
    [JsonObject(MemberSerialization.OptIn)]
    public partial struct Fix64 : IEquatable<Fix64>, IComparable<Fix64>, IComparable
    {
        /// <summary>
        /// The underlying integer representation
        /// </summary>
        [JsonProperty]
        public long RawValue => m_rawValue;

        readonly long m_rawValue;

        // Precision of this type is 2^-32, that is 2,3283064365386962890625E-10
        internal static readonly long SMALL_VALUE = WEIRD_BIT + 1L;
        public static readonly decimal Precision = (decimal)(new Fix64(2L));//0.00000000023283064365386962890625m;
        public static readonly Fix64 SmallValue = new Fix64(SMALL_VALUE);
        public static readonly Fix64 MaxValue = new Fix64(MAX_VALUE);
        public static readonly Fix64 MinValue = new Fix64(MIN_VALUE);
        public static readonly Fix64 InfPosValue = new Fix64(INF_POS);
        public static readonly Fix64 InfNegValue = new Fix64(INF_NEG);
        public static readonly Fix64 NaNValue = new Fix64(NaN);
        public static readonly Fix64 One = new Fix64(ONE);
        public static readonly Fix64 Two = new Fix64(TWO);
        public static readonly Fix64 Zero = new Fix64();
        /// <summary>
        /// The value of Pi
        /// </summary>
        public static readonly Fix64 Pi = Fix64_ForceSafe(PI);
        public static readonly Fix64 PiOver2 = Fix64_ForceSafe(PI_OVER_2);
        public static readonly Fix64 PiTimes2 = Fix64_ForceSafe(PI_TIMES_2);
        public static readonly Fix64 PiInv = Fix64_ForceSafe((Fix64)0.3183098861837906715377675267M);
        public static readonly Fix64 PiOver2Inv = Fix64_ForceSafe((Fix64)0.6366197723675813430755350535M);
        static readonly Fix64 Log2Max = Fix64_ForceSafe(LOG2MAX);
        static readonly Fix64 Log2Min = Fix64_ForceSafe(LOG2MIN);
        public static readonly Fix64 Ln2 = Fix64_ForceSafe(LN2);
        public static readonly Fix64 E = Fix64_ForceSafe(e);

        static readonly Fix64 LutInterval = (Fix64)(LUT_SIZE - 1) / PiOver2;

        public const long WEIRD_BIT = 1L;//The lowest bit is reserved for "broken" tags.

        internal const long NaN = (long.MaxValue - 2) | WEIRD_BIT;

        internal const long INF_POS = long.MaxValue | WEIRD_BIT;
        internal const long INF_NEG = long.MinValue | WEIRD_BIT;

        internal const long MAX_VALUE = long.MaxValue & ~WEIRD_BIT;
        internal const long MIN_VALUE = long.MinValue & ~WEIRD_BIT;


        public const long SIGN_BIT = 1L << 63;

        const int NUM_BITS = 64;
        const int FRACTIONAL_PLACES = 32;
        const long ONE = 1L << FRACTIONAL_PLACES;
        const long TWO = 1L << (FRACTIONAL_PLACES + 1);
        const long PI_TIMES_2 = 0x6487ED511;
        const long PI = 0x3243F6A88;
        const long PI_OVER_2 = 0x1921FB544;
        const long LN2 = 0xB17217F7;
        const long e = 11674931554L;
        const long LOG2MAX = 0x1F00000000;
        const long LOG2MIN = -0x2000000000;
        const int LUT_SIZE = (int)(PI_OVER_2 >> 15);

        /// <summary>
        /// Returns a number indicating the sign of a Fix64 number.
        /// Returns 1 if the value is positive, 0 if is 0, and -1 if it is negative.
        /// </summary>
        public static int Sign(Fix64 value)
        {
            return
                value.m_rawValue < 0 ? -1 :
                value.m_rawValue > 0 ? 1 :
                0;
        }


        /// <summary>
        /// Returns the absolute value of a Fix64 number.
        /// Note: Abs(Fix64.MinValue) == Fix64.MaxValue.
        /// </summary>
        public static Fix64 Abs(Fix64 value)
        {
            if (value.m_rawValue == MIN_VALUE)
                return MaxValue;

            // branchless implementation, see http://www.strchr.com/optimized_abs_function
            var mask = value.m_rawValue >> 62; //Only go down 62 bits because bit 0 is used for "weird" values
            return new Fix64((value.m_rawValue + mask) ^ mask);
        }

        public static bool IsNaN(Fix64 value) => value.m_rawValue == NaN || ((value.m_rawValue & WEIRD_BIT) != 0 && !IsInf(value));

        public static bool IsInf(Fix64 value) => value.m_rawValue == INF_NEG || value.m_rawValue == INF_POS;
        public static bool IsPositiveInf(Fix64 value) => value.m_rawValue == INF_POS;
        public static bool IsNegativeInf(Fix64 value) => value.m_rawValue == INF_NEG;

        public static bool IsNumber(Fix64 value) => (value.m_rawValue & WEIRD_BIT) == 0;
        public static bool IsNumber(long value) => (value & WEIRD_BIT) == 0;

        /// <summary>
        /// Returns the absolute value of a Fix64 number.
        /// FastAbs(Fix64.MinValue) is undefined.
        /// </summary>
        public static Fix64 FastAbs(Fix64 value)
        {
            // branchless implementation, see http://www.strchr.com/optimized_abs_function
            var mask = value.m_rawValue >> 62;
            return new Fix64((value.m_rawValue + mask) ^ mask);
        }


        /// <summary>
        /// Returns the largest integer less than or equal to the specified number.
        /// </summary>
        public static Fix64 Floor(Fix64 value) => new Fix64((long)((ulong)value.m_rawValue & 0xFFFFFFFF00000000));// Just zero out the fractional part 

        public static int FloorToInt(Fix64 value) => (int)Floor(value);

        /// <summary>
        /// Returns the smallest integral value that is greater than or equal to the specified number.
        /// </summary>
        public static Fix64 Ceiling(Fix64 value)
        {
            if (value.m_rawValue == MAX_VALUE) return value;
            var hasFractionalPart = (value.m_rawValue & 0x00000000FFFFFFFF) != 0;
            return hasFractionalPart ? Floor(value) + One : value;
        }

        public static int CeilToInt(Fix64 value) => (int)Ceiling(value);

        public bool IsInt => (m_rawValue & 0x00000000FFFFFFFF) == 0;

        /// <summary>
        /// Rounds a value to the nearest integral value.
        /// If the value is halfway between an even and an uneven value, returns the even value.
        /// </summary>
        public static Fix64 Round(Fix64 value)
        {
            if (value.m_rawValue == MAX_VALUE) return value;
            var fractionalPart = value.m_rawValue & 0x00000000FFFFFFFF;
            var integralPart = Floor(value);
            if (fractionalPart < 0x80000000)
            {
                return integralPart;
            }
            if (fractionalPart > 0x80000000)
            {
                return integralPart + One;
            }
            // if number is halfway between two values, round to the nearest even number
            // this is the method used by System.Math.Round().
            return (integralPart.m_rawValue & ONE) == 0
                       ? integralPart
                       : integralPart + One;
        }

        /// <summary>
        /// Adds x and y. Performs saturating addition, i.e. in case of overflow, 
        /// rounds to MinValue or MaxValue depending on sign of operands.
        /// </summary>
        public static Fix64 operator +(Fix64 x, Fix64 y)
        {
            var xl = x.m_rawValue;
            var yl = y.m_rawValue;

            //Check if either of these values was already "weird"
            if (!IsNumber(xl | yl))
                return WeirdSum(x, y);

            var sum = xl + yl;
            // if signs of operands are equal and signs of sum and x are different
            if (((~(xl ^ yl) & (xl ^ sum)) & SIGN_BIT) != 0)
            {
#if FIX64_CHECKED && !FIX64_CHECKED_OPERATOR
                throw new OverflowException($"Overflow performing {x} + {y}");
#else
                sum = xl > 0 ? InfPosValue : InfNegValue;
#endif
            }
            return new Fix64(sum);
        }

        public static Fix64 UnsafeAdd(Fix64 x, Fix64 y, bool check)
        {
            if (check) return x + y;
            var xl = x.m_rawValue;
            var yl = y.m_rawValue;

            //Check if either of these values was already "weird"
            if (!IsNumber(xl | yl))
                return WeirdSum(x, y);

            var sum = xl + yl;
            // if signs of operands are equal and signs of sum and x are different
            if (((~(xl ^ yl) & (xl ^ sum)) & SIGN_BIT) != 0)
            {
#if FIX64_CHECKED && !FIX64_CHECKED_OPERATOR
                if (check)
                    throw new OverflowException($"Overflow performing {x} + {y}");
                else
                    return xl > 0 ? InfPosValue : InfNegValue;
#else
                return xl > 0 ? InfPosValue : InfNegValue;
#endif
            }
            return new Fix64(sum);
        }

#if FIX64_CHECKED_OPERATOR
        /// <summary>
        /// Adds x and y. Performs saturating addition, i.e. in case of overflow, 
        /// rounds to MinValue or MaxValue depending on sign of operands.
        /// </summary>
        public static Fix64 operator checked +(Fix64 x, Fix64 y)
        {
            var xl = x.m_rawValue;
            var yl = y.m_rawValue;

            //Check if either of these values was already "weird"
            if (!IsNumber(xl | yl))
                return WeirdSum(x, y);

            var sum = xl + yl;
            // if signs of operands are equal and signs of sum and x are different
            if (((~(xl ^ yl) & (xl ^ sum)) & SIGN_BIT) != 0)
            {
#if FIX64_CHECKED
                throw new OverflowException($"Overflow performing {x} + {y}");
#else
                return xl > 0 ? InfPosValue : InfNegValue;
#endif
            }
            return new Fix64(sum);
        }
#endif

        /// <summary>
        /// Adds x and y witout performing overflow checking. Should be inlined by the CLR.
        /// </summary>
        public static Fix64 FastAdd(Fix64 x, Fix64 y)
        {
            return new Fix64(x.m_rawValue + y.m_rawValue);
        }

        /// <summary>
        /// Subtracts y from x. Performs saturating substraction, i.e. in case of overflow, 
        /// rounds to MinValue or MaxValue depending on sign of operands.
        /// </summary>
        public static Fix64 operator -(Fix64 x, Fix64 y)
        {
            if (!IsNumber(x.m_rawValue | y.m_rawValue))
                return WeirdSum(x, y);

#if FIX64_CHECKED && !FIX64_CHECKED_OPERATOR
            return UnsafeSub(x, y, true);
#else
            return UnsafeSub(x, y, false);
#endif
        }

#if FIX64_CHECKED_OPERATOR
        /// <summary>
        /// Subtracts y from x. Performs saturating substraction, i.e. in case of overflow, 
        /// rounds to MinValue or MaxValue depending on sign of operands.
        /// </summary>
        public static Fix64 operator checked -(Fix64 x, Fix64 y)
        {
            if (!IsNumber(x.m_rawValue | y.m_rawValue))
                return WeirdSum(x, y);

            return UnsafeSub(x, y, true);
        }
#endif

        public static Fix64 UnsafeSub(Fix64 x, Fix64 y, bool check)
        {
            var xl = x.m_rawValue;
            var yl = y.m_rawValue;

            var diff = xl - yl;
            // if signs of operands are different and signs of sum and x are different
            if ((((xl ^ yl) & (xl ^ diff)) & SIGN_BIT) != 0) //Also overflow if the WEIRD_BIT is triggered.
            {
#if FIX64_CHECKED
                if (check)
                    throw new OverflowException($"Overflow performing {x} - {y}");
#endif
                diff = xl < 0 ? MIN_VALUE : MAX_VALUE;
            }
            return Fix64_ForceSafe(diff);
        }

        /// <summary>
        /// Subtracts y from x witout performing overflow checking. Should be inlined by the CLR.
        /// </summary>
        public static Fix64 FastSub(Fix64 x, Fix64 y)
        {
            return new Fix64(x.m_rawValue - y.m_rawValue);
        }

        static long AddOverflowHelper(long x, long y, ref bool overflow)
        {
            var sum = x + y;
            // x + y overflows if sign(x) ^ sign(y) != sign(sum)
            overflow |= ((x ^ y ^ sum) & SIGN_BIT) != 0;
            return sum;
        }

        public static Fix64 UnsafeMulti(Fix64 x, Fix64 y, bool check)
        {
            var xl = x.m_rawValue == MIN_VALUE ? MAX_VALUE : Math.Abs(x.m_rawValue);
            var yl = y.m_rawValue == MIN_VALUE ? MAX_VALUE : Math.Abs(y.m_rawValue);

            bool resultSign = ((x.m_rawValue ^ y.m_rawValue) & SIGN_BIT) != 0;

            var xlo = (ulong)(xl & 0x00000000FFFFFFFF);
            var xhi = xl >> FRACTIONAL_PLACES;
            var ylo = (ulong)(yl & 0x00000000FFFFFFFF);
            var yhi = yl >> FRACTIONAL_PLACES;

            var lolo = xlo * ylo;
            var lohi = (long)xlo * yhi;
            var hilo = xhi * (long)ylo;
            var hihi = xhi * yhi;

            var loResult = lolo >> FRACTIONAL_PLACES;
            var midResult1 = lohi;
            var midResult2 = hilo;
            var hiResult = hihi << FRACTIONAL_PLACES;

            bool overflow = false;
            var sum = AddOverflowHelper((long)loResult, midResult1, ref overflow);
            sum = AddOverflowHelper(sum, midResult2, ref overflow);
            sum = AddOverflowHelper(sum, hiResult, ref overflow);

            // if signs of operands are equal and sign of result is negative,
            // then multiplication overflowed positively
            // the reverse is also true
            if (sum < 0)
            {
#if FIX64_CHECKED
                if (check)
                    throw new OverflowException($"Overflow performing {x} * {y}");
#endif
                if (resultSign)
                    return MinValue;
                else
                    return MaxValue;
            }

            // if the top 32 bits of hihi (unused in the result) are neither all 0s or 1s,
            // then this means the result overflowed.
            var topCarry = hihi >> FRACTIONAL_PLACES;
            if (topCarry != 0 && topCarry != -1 /*&& xl != -17 && yl != -17*/)
            {
#if FIX64_CHECKED
                if (check)
                    throw new OverflowException($"Overflow performing {x} * {y}");
#endif
                return resultSign ? MinValue : MaxValue;
            }

            // If signs differ, both operands' magnitudes are greater than 1,
            // and the result is greater than the negative operand, then there was negative overflow.
            if (resultSign)
            {
                long posOp, negOp;
                if (xl > yl)
                {
                    posOp = xl;
                    negOp = yl;
                }
                else
                {
                    posOp = yl;
                    negOp = xl;
                }
                if (sum > negOp && negOp < -ONE && posOp > ONE)
                {
#if FIX64_CHECKED
                    if (check)
                        throw new OverflowException($"Overflow performing {x} * {y}");
#endif
                    return MinValue;
                }
            }

            return Fix64_ForceSafe(resultSign ? -sum : sum);
        }

        public static Fix64 operator *(Fix64 x, Fix64 y)
        {
            if (!IsNumber(x.m_rawValue | y.m_rawValue))
                return WeirdMulti(x, y);

#if FIX64_CHECKED &&  !FIX64_CHECKED_OPERATOR
            return UnsafeMulti(x, y, true);
#else
            return UnsafeMulti(x, y, false);
#endif
        }
#if FIX64_CHECKED_OPERATOR
        public static Fix64 operator checked *(Fix64 x, Fix64 y)
        {
            if (!IsNumber(x.m_rawValue | y.m_rawValue))
                return WeirdMulti(x, y);
            return UnsafeMulti(x, y, true);
        }
#endif

        private static Fix64 WeirdSum(Fix64 x, Fix64 y) => IsNumber(x) ? y : x;

        private static Fix64 WeirdMulti(Fix64 x, Fix64 y)
        {
            var xl = x.m_rawValue;
            var yl = y.m_rawValue;

            bool isNegative = ((xl ^ yl) & SIGN_BIT) != 0;

            if (IsNaN(x) || IsNaN(y))
                return NaNValue;

            if (IsInf(x) || IsInf(y))
            {
                if (xl == 0 || yl == 0)
                    return 0;
                else
                    return isNegative ? InfNegValue : InfPosValue;
            }

            return NaNValue;
        }

        private static Fix64 WeirdDiv(Fix64 x, Fix64 y)
        {
            var xl = x.m_rawValue;
            var yl = y.m_rawValue;

            bool isNegative = ((xl ^ yl) & SIGN_BIT) != 0;

            if (IsNaN(x) || IsNaN(y) || y == Zero || (IsInf(x) && IsInf(y)))
                return NaNValue;

            if (IsInf(y))
            {
                return 0;
            }

            if (IsInf(x))
            {
                return isNegative ? InfNegValue : InfPosValue;
            }

            return NaNValue;
        }

        /// <summary>
        /// Performs multiplication without checking for overflow.
        /// Useful for performance-critical code where the values are guaranteed not to cause overflow
        /// </summary>
        public static Fix64 FastMul(Fix64 x, Fix64 y)
        {

            var xl = x.m_rawValue;
            var yl = y.m_rawValue;

            var xlo = (ulong)(xl & 0x00000000FFFFFFFF);
            var xhi = xl >> FRACTIONAL_PLACES;
            var ylo = (ulong)(yl & 0x00000000FFFFFFFF);
            var yhi = yl >> FRACTIONAL_PLACES;

            var lolo = xlo * ylo;
            var lohi = (long)xlo * yhi;
            var hilo = xhi * (long)ylo;
            var hihi = xhi * yhi;

            var loResult = lolo >> FRACTIONAL_PLACES;
            var midResult1 = lohi;
            var midResult2 = hilo;
            var hiResult = hihi << FRACTIONAL_PLACES;

            var sum = (long)loResult + midResult1 + midResult2 + hiResult;
            return Fix64_ForceSafe(sum);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        static int CountLeadingZeroes(ulong x)
        {
            int result = 0;
            while ((x & 0xF000000000000000) == 0) { result += 4; x <<= 4; }
            while ((x & 0x8000000000000000) == 0) { result += 1; x <<= 1; }
            return result;
        }

        public static Fix64 Div(Fix64 x, Fix64 y)
        {
            return x / y;
        }

        public static Fix64 UnsafeDiv(Fix64 x, Fix64 y, bool check)
        {
            var xl = x.m_rawValue;
            var yl = y.m_rawValue;

            if (yl == 0)
                return NaNValue;

            var remainder = (ulong)(xl >= 0 ? xl : -xl);
            var divider = (ulong)(yl >= 0 ? yl : -yl);
            var quotient = 0UL;
            var bitPos = NUM_BITS / 2 + 1;


            // If the divider is divisible by 2^n, take advantage of it.
            while ((divider & 0xF) == 0 && bitPos >= 4)
            {
                divider >>= 4;
                bitPos -= 4;
            }

            while (remainder != 0 && bitPos >= 0)
            {
                int shift = CountLeadingZeroes(remainder);
                if (shift > bitPos)
                {
                    shift = bitPos;
                }
                remainder <<= shift;
                bitPos -= shift;

                var div = remainder / divider;
                remainder = remainder % divider;
                quotient += div << bitPos;

                // Detect overflow
                if ((div & ~(0xFFFFFFFFFFFFFFFF >> bitPos)) != 0)
                {
#if FIX64_CHECKED
                    if (check)
                        throw new OverflowException($"Overflow performing {x} / {y}");
#endif
                    return ((xl ^ yl) & SIGN_BIT) == 0 ? InfPosValue : InfNegValue;
                }

                remainder <<= 1;
                --bitPos;
            }

            // rounding
            ++quotient;
            var result = (long)(quotient >> 1);
            if (((xl ^ yl) & SIGN_BIT) != 0)
            {
                result = -result;
            }

            return Fix64_ForceSafe(result);
        }

        public static Fix64 operator /(Fix64 x, Fix64 y)
        {
            var xl = x.m_rawValue;
            var yl = y.m_rawValue;

            if (yl == 0)
            {
                if (IsNaN(x)) return NaNValue;
                return (((xl ^ yl) & SIGN_BIT) == 0) ? InfPosValue : InfNegValue;
            }

            if (!IsNumber(xl | yl))
                return WeirdDiv(x, y);

#if FIX64_CHECKED && !FIX64_CHECKED_OPERATOR
            return UnsafeDiv(x, y, true);
#else
            return UnsafeDiv(x, y, false);
#endif
        }
        public static Fix64 DivOrDefault(Fix64 x, Fix64 y, Fix64 def = default)
        {
            var xl = x.m_rawValue;
            var yl = y.m_rawValue;

            Fix64 result;
            if (yl == 0)
            {
                if (IsNaN(x)) result = NaNValue;
                else result = (((xl ^ yl) & SIGN_BIT) == 0) ? InfPosValue : InfNegValue;
            }
            else if (!IsNumber(xl | yl))
                result = WeirdDiv(x, y);
            else
                result = UnsafeDiv(x, y, false);

            if (Fix64.IsNumber(result))
                return result;
            else
                return def;
        }

#if FIX64_CHECKED_OPERATOR
        public static Fix64 operator checked /(Fix64 x, Fix64 y)
        {
            var xl = x.m_rawValue;
            var yl = y.m_rawValue;

            if (yl == 0)
            {
                if (IsNaN(x)) return NaNValue;
                return (((xl ^ yl) & SIGN_BIT) == 0) ? InfPosValue : InfNegValue;
            }

            if (!IsNumber(xl | yl))
                return WeirdDiv(x, y);

            return UnsafeDiv(x, y, true);
        }
#endif

        public static Fix64 operator %(Fix64 x, Fix64 y)
        {
            if (!IsNumber(x.m_rawValue | y.m_rawValue))
                return NaNValue;

            if (y.m_rawValue == 0) return NaNValue;

            return Fix64_ForceSafe(
                x.m_rawValue == MIN_VALUE & y.m_rawValue == -2 ?
                0 :
                x.m_rawValue % y.m_rawValue);
        }

        /// <summary>
        /// Performs modulo as fast as possible; throws if x == MinValue and y == -1.
        /// Use the operator (%) for a more reliable but slower modulo.
        /// </summary>
        public static Fix64 FastMod(Fix64 x, Fix64 y)
        {
            return Fix64_ForceSafe(x.m_rawValue % y.m_rawValue);
        }

        public static Fix64 operator -(Fix64 x)
        {
            return IsNumber(x) ?
                (x.m_rawValue == MIN_VALUE ? MaxValue : new Fix64(-x.m_rawValue))
                : new Fix64(x.m_rawValue ^ SIGN_BIT);
        }
        public static Fix64 operator ++(Fix64 x)
        {
            return x + One;
        }
        public static Fix64 operator --(Fix64 x)
        {
            return x - One;
        }

        public static bool operator ==(Fix64 x, Fix64 y)
        {
            return x.m_rawValue == y.m_rawValue && !(IsNaN(x) || IsNaN(y));
        }

        public static bool operator !=(Fix64 x, Fix64 y)
        {
            return x.m_rawValue != y.m_rawValue || IsNaN(x) || IsNaN(y);
        }

        public static bool operator >(Fix64 x, Fix64 y)
        {
            if (IsPositiveInf(x)) return x.m_rawValue != y.m_rawValue && !IsNaN(x) && !IsNaN(y);
            if (IsNegativeInf(y)) return x.m_rawValue != y.m_rawValue && !IsNaN(x) && !IsNaN(y);
            if (IsNegativeInf(x)) return false;
            if (IsPositiveInf(y)) return false;

            return x.m_rawValue > y.m_rawValue && !(IsNaN(x) || IsNaN(y));
        }

        public static bool operator <(Fix64 x, Fix64 y)
        {
            if (IsNegativeInf(x)) return x.m_rawValue != y.m_rawValue && !IsNaN(x) && !IsNaN(y);
            if (IsPositiveInf(y)) return x.m_rawValue != y.m_rawValue && !IsNaN(x) && !IsNaN(y);
            if (IsPositiveInf(x)) return false;
            if (IsNegativeInf(y)) return false;

            return x.m_rawValue < y.m_rawValue && !(IsNaN(x) || IsNaN(y));
        }

        public static bool operator >=(Fix64 x, Fix64 y)
        {
            if (IsPositiveInf(x)) return !IsNaN(x) && !IsNaN(y);
            if (IsNegativeInf(y)) return !IsNaN(x) && !IsNaN(y);
            if (IsNegativeInf(x)) return x.m_rawValue == y.m_rawValue;
            if (IsPositiveInf(y)) return x.m_rawValue == y.m_rawValue;

            return x.m_rawValue >= y.m_rawValue && !(IsNaN(x) || IsNaN(y));
        }

        public static bool operator <=(Fix64 x, Fix64 y)
        {
            if (IsNegativeInf(x)) return !IsNaN(x) && !IsNaN(y);
            if (IsPositiveInf(y)) return !IsNaN(x) && !IsNaN(y);
            if (IsPositiveInf(x)) return x.m_rawValue == y.m_rawValue;
            if (IsNegativeInf(y)) return x.m_rawValue == y.m_rawValue;

            return x.m_rawValue <= y.m_rawValue && !(IsNaN(x) || IsNaN(y));
        }


        public static bool operator ==(Fix64 x, int y)
        {
            return x == (Fix64)y;
        }
        public static bool operator !=(Fix64 x, int y)
        {
            return x != (Fix64)y;
        }
        public static bool operator >(Fix64 x, int y)
        {
            return x > (Fix64)y;
        }
        public static bool operator <(Fix64 x, int y)
        {
            return x < (Fix64)y;
        }
        public static bool operator >=(Fix64 x, int y)
        {
            return x >= (Fix64)y;
        }
        public static bool operator <=(Fix64 x, int y)
        {
            return x <= (Fix64)y;
        }

        /// <summary>
        /// Returns 2 raised to the specified power.
        /// Provides at least 6 decimals of accuracy.
        /// </summary>
        internal static Fix64 Pow2(Fix64 x)
        {
            if (x.m_rawValue == 0)
            {
                return One;
            }

            // Avoid negative arguments by exploiting that exp(-x) = 1/exp(x).
            bool neg = x.m_rawValue < 0;
            if (neg)
            {
                x = -x;
            }

            if (x == One)
            {
                return neg ? One / (Fix64)2 : (Fix64)2;
            }
            if (x >= Log2Max)
            {
                return neg ? One / MaxValue : MaxValue;
            }
            if (x <= Log2Min)
            {
                return neg ? MaxValue : Zero;
            }

            /* The algorithm is based on the power series for exp(x):
             * http://en.wikipedia.org/wiki/Exponential_function#Formal_definition
             * 
             * From term n, we get term n+1 by multiplying with x/n.
             * When the sum term drops to zero, we can stop summing.
             */

            int integerPart = (int)Floor(x);
            // Take fractional part of exponent
            x = new Fix64(x.m_rawValue & 0x00000000FFFFFFFF);

            var result = One;
            var term = One;
            int i = 1;
            while (term.m_rawValue != 0)
            {
                term = UnsafeDiv(FastMul(FastMul(x, term), Ln2), (Fix64)i, false);
                result += term;
                i++;
            }

            result = Fix64_ForceSafe(result.m_rawValue << integerPart);
            if (neg)
            {
                result = One / result;
            }

            return result;
        }

        /// <summary>
        /// Returns the base-2 logarithm of a specified number.
        /// Provides at least 9 decimals of accuracy.
        /// </summary>
        internal static Fix64 Log2(Fix64 x)
        {
            if (x.m_rawValue <= 0)
            {
                return NaNValue;
            }
            if (!IsNumber(x))
                return x;

            // This implementation is based on Clay. S. Turner's fast binary logarithm
            // algorithm (C. S. Turner,  "A Fast Binary Logarithm Algorithm", IEEE Signal
            //     Processing Mag., pp. 124,140, Sep. 2010.)

            long b = 1U << (FRACTIONAL_PLACES - 1);
            long y = 0;

            long rawX = x.m_rawValue;
            while (rawX < ONE)
            {
                rawX <<= 1;
                y -= ONE;
            }

            while (rawX >= (ONE << 1))
            {
                rawX >>= 1;
                y += ONE;
            }

            var z = new Fix64(rawX);

            for (int i = 0; i < FRACTIONAL_PLACES; i++)
            {
                z = FastMul(z, z);
                if (z.m_rawValue >= (ONE << 1))
                {
                    z = new Fix64(z.m_rawValue >> 1);
                    y += b;
                }
                b >>= 1;
            }

            return Fix64_ForceSafe(y);
        }

        private static Fix64 Fix64_ForceSafe(Fix64 y) => new Fix64(y.m_rawValue & ~WEIRD_BIT);
        private static Fix64 Fix64_ForceSafe(long y) => new Fix64(y & ~WEIRD_BIT);

        /// <summary>
        /// Returns the natural logarithm of a specified number.
        /// Provides at least 7 decimals of accuracy.
        /// </summary>
        public static Fix64 Ln(Fix64 x)
        {
            return Log2(x) * Ln2;
        }

        public static Fix64 Exp(Fix64 x)
        {
            return Pow(x, E);
        }

        /// <summary>
        /// Returns a specified number raised to the specified power.
        /// Provides about 5 digits of accuracy for the result.
        /// </summary>
        public static Fix64 Pow(Fix64 b, Fix64 exp)
        {
            if (!IsNumber(b.m_rawValue | exp.m_rawValue))
                return WeirdPow(b, exp);

            if (b == One)
            {
                return One;
            }
            if (exp.m_rawValue == 0)
            {
                return One;
            }
            if (b.m_rawValue == 0)
            {
                if (exp.m_rawValue < 0)
                {
                    return NaNValue;
                }
                return Zero;
            }

            //Do a quick pow for natural numbers less than 5.
            if (b.IsInt && exp.IsInt && exp > 0 && exp < 5)
            {
                Fix64 outVal = b;
                for (int i = 1; i < exp; i++)
                    outVal = Fix64.UnsafeMulti(outVal, b, false);
                return outVal;
            }

            if (b.m_rawValue < 0)
            {
                if (exp.IsInt) //We can only do pow of negative numbers if the exponent is an integer.
                {
                    Fix64 log2 = Log2(-b);
                    Fix64 result = Pow2(Fix64.UnsafeMulti(exp, log2, false));
                    return exp % Two == Zero ? result : -result;
                }
                else
                    return NaNValue;
            }
            else
            {
                Fix64 log2 = Log2(b);
                return Pow2(Fix64.UnsafeMulti(exp, log2, false));
            }
        }

        internal static Fix64 WeirdPow(Fix64 b, Fix64 exp)
        {
            if (IsNaN(b) || IsNaN(exp))
            {
                return NaNValue;
            }
            if (b.m_rawValue == 0 && exp.m_rawValue < 0)
                return NaNValue;
            if (exp.m_rawValue == 0)
                return One;
            if (IsPositiveInf(b))
            {
                return exp.m_rawValue > 0 ? InfPosValue : 0;
            }
            if (IsNegativeInf(b))
            {
                return exp.IsInt ? (exp % Two == Zero ? InfPosValue : InfNegValue) : NaNValue;
            }
            if (IsPositiveInf(exp))
            {
                return b.m_rawValue > 0 ? InfPosValue : NaNValue;
            }
            if (IsNegativeInf(exp))
            {
                return b.m_rawValue > 0 ? 0 : NaNValue;
            }
            return NaNValue;
        }

        /// <summary>
        /// Returns the square root of a specified number.
        /// </summary>
        public static Fix64 Sqrt(Fix64 x)
        {
            var xl = x.m_rawValue;
            if (xl < 0)
                return NaNValue;
            else if (!IsNumber(xl))
                return x;

            var num = (ulong)xl;
            var result = 0UL;

            // second-to-top bit
            var bit = 1UL << (NUM_BITS - 2);

            while (bit > num)
            {
                bit >>= 2;
            }

            // The main part is executed twice, in order to avoid
            // using 128 bit values in computations.
            for (var i = 0; i < 2; ++i)
            {
                // First we get the top 48 bits of the answer.
                while (bit != 0)
                {
                    if (num >= result + bit)
                    {
                        num -= result + bit;
                        result = (result >> 1) + bit;
                    }
                    else
                    {
                        result = result >> 1;
                    }
                    bit >>= 2;
                }

                if (i == 0)
                {
                    // Then process it again to get the lowest 16 bits.
                    if (num > (1UL << (NUM_BITS / 2)) - 1)
                    {
                        // The remainder 'num' is too large to be shifted left
                        // by 32, so we have to add 1 to result manually and
                        // adjust 'num' accordingly.
                        // num = a - (result + 0.5)^2
                        //       = num + result^2 - (result + 0.5)^2
                        //       = num - result - 0.5
                        num -= result;
                        num = (num << (NUM_BITS / 2)) - 0x80000000UL;
                        result = (result << (NUM_BITS / 2)) + 0x80000000UL;
                    }
                    else
                    {
                        num <<= (NUM_BITS / 2);
                        result <<= (NUM_BITS / 2);
                    }

                    bit = 1UL << (NUM_BITS / 2 - 2);
                }
            }
            // Finally, if next bit would have been 1, round the result upwards.
            if (num > result)
            {
                ++result;
            }
            return Fix64_ForceSafe((long)result);
        }

        /// <summary>
        /// Returns the Sine of x.
        /// The relative error is less than 1E-10 for x in [-2PI, 2PI], and less than 1E-7 in the worst case.
        /// </summary>
        public static Fix64 Sin(Fix64 x)
        {
            if (!IsNumber(x))
                return NaNValue;

            var clampedL = ClampSinValue(x.m_rawValue, out var flipHorizontal, out var flipVertical);
            var clamped = new Fix64(clampedL);

            // Find the two closest values in the LUT and perform linear interpolation
            // This is what kills the performance of this function on x86 - x64 is fine though
            var rawIndex = FastMul(clamped, LutInterval);
            var roundedIndex = Round(rawIndex);
            var indexError = FastSub(rawIndex, roundedIndex);

            var nearestValue = new Fix64(SinLut[flipHorizontal ?
                SinLut.Length - 1 - (int)roundedIndex :
                (int)roundedIndex]);
            var secondNearestValue = new Fix64(SinLut[flipHorizontal ?
                SinLut.Length - 1 - (int)roundedIndex - Sign(indexError) :
                (int)roundedIndex + Sign(indexError)]);

            var delta = FastMul(indexError, FastAbs(FastSub(nearestValue, secondNearestValue))).m_rawValue;
            var interpolatedValue = nearestValue.m_rawValue + (flipHorizontal ? -delta : delta);
            var finalValue = flipVertical ? -interpolatedValue : interpolatedValue;
            return Fix64_ForceSafe(finalValue);
        }

        public static Fix64 Lerp(Fix64 a, Fix64 b, Fix64 t)
        {
            return a + t * (b - a);
        }

        /// <summary>
        /// Returns a rough approximation of the Sine of x.
        /// This is at least 3 times faster than Sin() on x86 and slightly faster than Math.Sin(),
        /// however its accuracy is limited to 4-5 decimals, for small enough values of x.
        /// </summary>
        public static Fix64 FastSin(Fix64 x)
        {
            if (!IsNumber(x))
                return NaNValue;

            var clampedL = ClampSinValue(x.m_rawValue, out bool flipHorizontal, out bool flipVertical);

            // Here we use the fact that the SinLut table has a number of entries
            // equal to (PI_OVER_2 >> 15) to use the angle to index directly into it
            var rawIndex = (uint)(clampedL >> 15);
            if (rawIndex >= LUT_SIZE)
            {
                rawIndex = LUT_SIZE - 1;
            }
            var nearestValue = SinLut[flipHorizontal ?
                SinLut.Length - 1 - (int)rawIndex :
                (int)rawIndex];
            return Fix64_ForceSafe(flipVertical ? -nearestValue : nearestValue);
        }


        static long ClampSinValue(long angle, out bool flipHorizontal, out bool flipVertical)
        {
            var largePI = 7244019458077122842;
            // Obtained from ((Fix64)1686629713.065252369824872831112M).m_rawValue
            // This is (2^29)*PI, where 29 is the largest N such that (2^N)*PI < MaxValue.
            // The idea is that this number contains way more precision than PI_TIMES_2,
            // and (((x % (2^29*PI)) % (2^28*PI)) % ... (2^1*PI) = x % (2 * PI)
            // In practice this gives us an error of about 1,25e-9 in the worst case scenario (Sin(MaxValue))
            // Whereas simply doing x % PI_TIMES_2 is the 2e-3 range.

            var clamped2Pi = angle;
            for (int i = 0; i < 29; ++i)
            {
                clamped2Pi %= (largePI >> i);
            }
            if (angle < 0)
            {
                clamped2Pi += PI_TIMES_2;
            }

            // The LUT contains values for 0 - PiOver2; every other value must be obtained by
            // vertical or horizontal mirroring
            flipVertical = clamped2Pi >= PI;
            // obtain (angle % PI) from (angle % 2PI) - much faster than doing another modulo
            var clampedPi = clamped2Pi;
            while (clampedPi >= PI)
            {
                clampedPi -= PI;
            }
            flipHorizontal = clampedPi >= PI_OVER_2;
            // obtain (angle % PI_OVER_2) from (angle % PI) - much faster than doing another modulo
            var clampedPiOver2 = clampedPi;
            if (clampedPiOver2 >= PI_OVER_2)
            {
                clampedPiOver2 -= PI_OVER_2;
            }
            return clampedPiOver2;
        }

        /// <summary>
        /// Returns the cosine of x.
        /// The relative error is less than 1E-10 for x in [-2PI, 2PI], and less than 1E-7 in the worst case.
        /// </summary>
        public static Fix64 Cos(Fix64 x)
        {
            var xl = x.m_rawValue;
            var rawAngle = xl + (xl > 0 ? -PI - PI_OVER_2 : PI_OVER_2);
            return Sin(new Fix64(rawAngle));
        }

        /// <summary>
        /// Returns a rough approximation of the cosine of x.
        /// See FastSin for more details.
        /// </summary>
        public static Fix64 FastCos(Fix64 x)
        {
            var xl = x.m_rawValue;
            var rawAngle = xl + (xl > 0 ? -PI - PI_OVER_2 : PI_OVER_2);
            return FastSin(new Fix64(rawAngle));
        }

        /// <summary>
        /// Returns the tangent of x.
        /// </summary>
        /// <remarks>
        /// This function is not well-tested. It may be wildly inaccurate.
        /// </remarks>
        public static Fix64 Tan(Fix64 x)
        {
            var clampedPi = x.m_rawValue % PI;
            var flip = false;
            if (clampedPi < 0)
            {
                clampedPi = -clampedPi;
                flip = true;
            }
            if (clampedPi > PI_OVER_2)
            {
                flip = !flip;
                clampedPi = PI_OVER_2 - (clampedPi - PI_OVER_2);
            }

            var clamped = new Fix64(clampedPi);

            // Find the two closest values in the LUT and perform linear interpolation
            var rawIndex = FastMul(clamped, LutInterval);
            var roundedIndex = Round(rawIndex);
            var indexError = FastSub(rawIndex, roundedIndex);

            var nearestValue = new Fix64(TanLut[(int)roundedIndex]);
            var secondNearestValue = new Fix64(TanLut[(int)roundedIndex + Sign(indexError)]);

            var delta = FastMul(indexError, FastAbs(FastSub(nearestValue, secondNearestValue))).m_rawValue;
            var interpolatedValue = nearestValue.m_rawValue + delta;
            var finalValue = flip ? -interpolatedValue : interpolatedValue;
            return Fix64_ForceSafe(finalValue);
        }

        /// <summary>
        /// Returns the arccos of of the specified number, calculated using Atan and Sqrt
        /// This function has at least 7 decimals of accuracy.
        /// </summary>
        public static Fix64 Acos(Fix64 x)
        {
            if (x < -One)
                return NaNValue;
            else if (x > One)
                return NaNValue;
            else if (!IsNumber(x))
                return x;

            if (x.m_rawValue == 0) return PiOver2;

            Fix64 mul = UnsafeMulti(x, x, false);
            Fix64 sub = UnsafeSub(One, mul, false);
            Fix64 root = Sqrt(sub);
            Fix64 div = UnsafeDiv(root, x, false);
            var result = Atan(div);
            return x.m_rawValue < 0 ? result + Pi : result;
        }

        /// <summary>
        /// Returns the arctan of of the specified number, calculated using Euler series
        /// This function has at least 7 decimals of accuracy.
        /// </summary>
        public static Fix64 Atan(Fix64 z)
        {
            if (z.m_rawValue == 0) return Zero;
            if (Fix64.IsPositiveInf(z)) return Fix64.PiOver2;
            else if (Fix64.IsNegativeInf(z)) return -Fix64.PiOver2;

            // Force positive values for argument
            // Atan(-z) = -Atan(z).
            var neg = z.m_rawValue < 0;
            if (neg)
            {
                z = -z;
            }

            Fix64 result;
            var two = (Fix64)2;
            var three = (Fix64)3;

            bool invert = z > One;
            if (invert) z = UnsafeDiv(One, z, false);

            result = One;
            var term = One;

            var zSq = UnsafeMulti(z, z, false);
            var zSq2 = UnsafeMulti(zSq, two, false);
            var zSqPlusOne = zSq + One;
            var zSq12 = UnsafeMulti(zSqPlusOne, two, false);
            var dividend = zSq2;
            var divisor = UnsafeMulti(zSqPlusOne, three, false);

            for (var i = 2; i < 30; ++i)
            {
                term = UnsafeMulti(term, UnsafeDiv(dividend, divisor, false), false);
                result += term;

                dividend += zSq2;
                divisor += zSq12;

                if (term.m_rawValue == 0) break;
            }

            result = UnsafeDiv(UnsafeMulti(result, z, false), zSqPlusOne, false);

            if (invert)
            {
                result = UnsafeSub(PiOver2, result, false);
            }

            if (neg)
            {
                result = -result;
            }
            return result;
        }

        public static Fix64 Atan2(Fix64 y, Fix64 x)
        {
            var yl = y.m_rawValue;
            var xl = x.m_rawValue;
            if (xl == 0)
            {
                if (yl > 0)
                {
                    return PiOver2;
                }
                if (yl == 0)
                {
                    return Zero;
                }
                //if (yl < 0)
                {
                    return -PiOver2;
                }
            }
            if (xl > 0)
                return Atan(Fix64.UnsafeDiv(y, x, false));
            else if (y >= 0)
                return Atan(Fix64.UnsafeDiv(y, x, false)) + Pi;
            else
                return Atan(Fix64.UnsafeDiv(y, x, false)) - Pi;

            ///One day ben will finish this.
            //Fix64 atan;
            //var z = UnsafeDiv(y, x);
            //var zz = UnsafeMulti(z,z);
            //var zeroTwoEight = FromRaw((long)(0.28M * ONE));

            //// Deal with overflow
            //if ((One + UnsafeMulti(zeroTwoEight, zz)) == MaxValue)
            //{
            //    return y < Zero ? -PiOver2 : PiOver2;
            //}

            //if (Abs(z) < One)
            //{
            //    atan = UnsafeDiv(z, (One + UnsafeMulti(zeroTwoEight, zz)));
            //    if (xl < 0)
            //    {
            //        if (yl < 0)
            //        {
            //            return UnsafeSub(atan, Pi);
            //        }
            //        return atan + Pi;
            //    }
            //}
            //else
            //{
            //    atan = UnsafeSub(PiOver2, UnsafeDiv(z, zz + zeroTwoEight));
            //    if (yl < 0)
            //    {
            //        return UnsafeSub(atan, Pi);
            //    }
            //}
            //return atan;
        }


        #region Implicit/Explicit conversions

#if FIX64_IMPLICIT_TO_ENABLED
        public static implicit operator Fix64(long value)
#else
        public static explicit operator Fix64(long value)
#endif
        {
            return Fix64_ForceSafe(value * ONE);
        }

#if FIX64_IMPLICIT_FROM_ENABLED
        public static implicit operator long(Fix64 value)
#else
        public static explicit operator long(Fix64 value)
#endif
        {
            return (value.m_rawValue >> FRACTIONAL_PLACES) + (value.m_rawValue < 0 && !value.IsInt ? 1 : 0);
        }

#if FIX64_IMPLICIT_TO_ENABLED
        public static implicit operator Fix64(float value)
#else
        public static explicit operator Fix64(float value)
#endif
        {
            if (float.IsNaN(value))
                return NaNValue;
            else if (float.IsPositiveInfinity(value))
                return InfPosValue;
            else if (float.IsNegativeInfinity(value))
                return InfNegValue;
            else if (value >= MaxValue)
                return MaxValue;
            else
                return Fix64_ForceSafe((long)(value * ONE));
        }

#if FIX64_IMPLICIT_FROM_ENABLED
        public static implicit operator float(Fix64 value)
#else
        public static explicit operator float(Fix64 value)
#endif
        {
            if (IsNaN(value))
                return float.NaN;
            else if (IsPositiveInf(value))
                return float.PositiveInfinity;
            else if (IsNegativeInf(value))
                return float.NegativeInfinity;
            else
                return (float)value.m_rawValue / ONE;
        }

#if FIX64_IMPLICIT_TO_ENABLED
        public static implicit operator Fix64(double value)
#else
        public static explicit operator Fix64(double value)
#endif
        {
            if (double.IsNaN(value))
                return NaNValue;
            else if (double.IsPositiveInfinity(value))
                return InfPosValue;
            else if (double.IsNegativeInfinity(value))
                return InfNegValue;
            else
                return Fix64_ForceSafe((long)(value * ONE));
        }

#if FIX64_IMPLICIT_FROM_ENABLED
        public static implicit operator double(Fix64 value)
#else
        public static explicit operator double(Fix64 value)
#endif
        {
            if (IsNaN(value))
                return double.NaN;
            else if (IsPositiveInf(value))
                return double.PositiveInfinity;
            else if (IsNegativeInf(value))
                return double.NegativeInfinity;
            else
                return (double)value.m_rawValue / ONE;
        }

#if FIX64_IMPLICIT_TO_ENABLED
        public static implicit operator Fix64(decimal value)
#else
        public static explicit operator Fix64(decimal value)
#endif
        {
            var val = value * ONE;
            if (val > Fix64.MAX_VALUE)
                return Fix64.InfPosValue;
            else if (val < Fix64.MIN_VALUE)
                return Fix64.InfNegValue;
            return Fix64_ForceSafe((long)(value * ONE));
        }

#if FIX64_IMPLICIT_FROM_ENABLED
        public static implicit operator decimal(Fix64 value)
#else
        public static explicit operator decimal(Fix64 value)
#endif
        {
            return (decimal)value.m_rawValue / ONE;
        }
        #endregion

        public static implicit operator Fix64(int value)
        {
            return new Fix64(value);
        }


        public override bool Equals(object obj)
        {
            return obj is Fix64 && ((Fix64)obj) == this;
        }

        public override int GetHashCode()
        {
            return m_rawValue.GetHashCode();
        }

        public bool Equals(Fix64 other)
        {
            return this == other;
        }

        public int CompareTo(Fix64 other)
        {
            if (this > other) return 1;
            else if (this == other) return 0;
            else if (IsNaN(this))
            {
                if (IsNaN(other)) return 0;
                else return 1;
            }
            else return -1;

        }

        public int CompareTo(object other)
        {
            if (other == null)
                return 1;

            if (other is Fix64 otherNumber)
            {
                return CompareTo(otherNumber);
            }
            else
            {
                throw new ArgumentException("Number is not a Fix64");
            }
        }

        public string ToJString()
        {
            // Up to 10 decimal places
            if (!IsNumber(m_rawValue))
            {
                if (IsPositiveInf(this))
                    return "+Inf";
                else if (IsNegativeInf(this))
                    return "-Inf";
                else
                    return "NaN";
            }
            return m_rawValue.ToString(Culture.NumberFormat) + "L";
        }

        public override string ToString() => ToString(format: "N2");
        public string ToString(IFormatProvider provider) => ToString(prov: provider);
        public string ToString(string format = "N2", IFormatProvider prov = null)
        {
            // Up to 10 decimal places
            if (!IsNumber(m_rawValue))
            {
                if (IsPositiveInf(this))
                    return "+Inf";
                else if (IsNegativeInf(this))
                    return "-Inf";
                else
                    return "NaN";
            }
            else
                return ((decimal)this).ToString(format, prov ?? Culture.NumberFormat);
        }

        public static Fix64 FromRaw(long rawValue)
        {
            return new Fix64(rawValue);
        }

        internal static void GenerateSinLut()
        {
            using (var writer = new StreamWriter("Fix64SinLut.cs"))
            {
                writer.Write(
    @"namespace FixMath.NET 
{
    partial struct Fix64 
    {
        public static readonly long[] SinLut = new[] 
        {");
                int lineCounter = 0;
                for (int i = 0; i < LUT_SIZE; ++i)
                {
                    var angle = i * Math.PI * 0.5 / (LUT_SIZE - 1);
                    if (lineCounter++ % 8 == 0)
                    {
                        writer.WriteLine();
                        writer.Write("            ");
                    }
                    var sin = Math.Sin(angle);
                    var rawValue = ((Fix64)sin).m_rawValue;
                    writer.Write(string.Format(Culture.NumberFormat, "0x{0:X}L, ", rawValue));
                }
                writer.Write(
    @"
        };
    }
}");
            }
        }

        internal static void GenerateTanLut()
        {
            using (var writer = new StreamWriter("Fix64TanLut.cs"))
            {
                writer.Write(
    @"namespace FixMath.NET 
{
    partial struct Fix64 
    {
        public static readonly long[] TanLut = new[] 
        {");
                int lineCounter = 0;
                for (int i = 0; i < LUT_SIZE; ++i)
                {
                    var angle = i * Math.PI * 0.5 / (LUT_SIZE - 1);
                    if (lineCounter++ % 8 == 0)
                    {
                        writer.WriteLine();
                        writer.Write("            ");
                    }
                    var tan = Math.Tan(angle);
                    if (tan > (double)MaxValue || tan < 0.0)
                    {
                        tan = (double)MaxValue;
                    }
                    var rawValue = (((decimal)tan > (decimal)MaxValue || tan < 0.0) ? MaxValue : (Fix64)tan).m_rawValue;
                    writer.Write(string.Format(Culture.NumberFormat, "0x{0:X}L, ", rawValue));
                }
                writer.Write(
    @"
        };
    }
}");
            }
        }

        // turn into a Console Application and use this to generate the look-up tables
        //static void Main(string[] args)
        //{
        //    GenerateSinLut();
        //    GenerateTanLut();
        //}


        /// <summary>
        /// This is the constructor from raw value; it can only be used interally.
        /// </summary>
        /// <param name="rawValue"></param>
        Fix64(long rawValue)
        {
            m_rawValue = rawValue;
        }

        public Fix64(int value)
        {
            m_rawValue = value * ONE;
        }

        public static Fix64 Max(Fix64 a, Fix64 b) => a > b ? a : b;
        public static Fix64 Max(Fix64 a, params Fix64[] p)
        {
            for (int i = 0; i < p.Length; i++)
                a = Max(a, p[i]);
            return a;
        }

        public static Fix64? Max(Fix64? a, Fix64? b)
        {
            if (a.HasValue && b.HasValue)
                return Max(a.Value, b.Value);
            else if (!a.HasValue)
                return b;
            else return a;
        }

        public static Fix64 Min(Fix64 a, Fix64 b) => a > b ? b : a;
        public static Fix64 Min(Fix64 a, params Fix64[] p)
        {
            for (int i = 0; i < p.Length; i++)
                a = Min(a, p[i]);
            return a;
        }

        public static Fix64 Clamp(Fix64 value, Fix64 min, Fix64 max) => Min(max, Max(min, value));

        public static bool TryParse(string v, out Fix64 val, IFormatProvider prov = null)
        {
            try
            {
                val = Parse(v, prov);
                return true;
            }
            catch
            {
                val = 0;
                return false;
            }
        }

        public static Fix64 Parse(string v, IFormatProvider prov = null)
        {
            if (string.IsNullOrWhiteSpace(v))
                return Fix64.Zero;

            switch (v)
            {
                case "+Inf":
                    return InfPosValue;
                case "-Inf":
                    return InfNegValue;
                case "NaN":
                    return NaNValue;
                case "+NaN":
                    return NaNValue;
                case "-NaN":
                    return NaNValue;
            }

            if (v.Contains(".") || v.Contains(","))
                return (Fix64)decimal.Parse(v, prov ?? Culture.NumberFormat);
            else if (v.EndsWith("L"))
                return FromRaw(long.Parse(v.Substring(0, v.Length - 1), prov ?? Culture.NumberFormat));
            else
                return (Fix64)long.Parse(v, prov ?? Culture.NumberFormat);
            //v = v.Trim(' '); //Clean off whitespace.

            //string[] split = v.Split('.');
            //if (split.Length > 2)
            //{
            //    throw new FormatException($"String {v} could not be parsed to Fix64. Accepts only 1 decimal point.");
            //}
            //else if (split.Length == 1)
            //{
            //    return new Fix64(int.Parse(v));
            //}
            //else //if (split.Length == 2)
            //{
            //    Fix64 upper = new Fix64(int.Parse(split[0]));

            //    //We want to trim off unecessary 0's from the end of this.
            //    string cleanedLower = split[1].TrimEnd('0');

            //    //We want to convert this cleaned up string into an integer value.
            //    //Then, convert it to a Fix64
            //    //Then, add it to the upper, after dividing it by the decimal count.
            //    Fix64 lowerIntValue = new Fix64(int.Parse(cleanedLower));

            //    //Figure out how much to divide the divisor by
            //    int lowerLength = cleanedLower.Length;
            //    int tenPower = 1;
            //    for (int i = 0; i < lowerLength; i++)
            //        tenPower *= 10;

            //    return upper + (lowerIntValue / new Fix64(tenPower));
            //}
        }

        public static Fix64 Convert(object value)
        {
            if (value == null)
                return Fix64.Zero;

            switch (value)
            {
                case string s:
                    return Fix64.Parse(s);
                case long l:
                    return (Fix64)l;
                case decimal D:
                    return (Fix64)D;
                case double d:
                    return (Fix64)d;
                case float f:
                    return (Fix64)f;
                case int i:
                    return (Fix64)i;
                default:
                    throw new NotImplementedException($"Couldn't convert object {value} of type {value.GetType()} to Fix64.");
            }
        }


    }
}
