﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace FixMath.NET
{
    public static class Culture
    {
        public static NumberFormatInfo NumberFormat
        {
            get
            {
                if (_numberFormat == null)
                {
                    _numberFormat = new CultureInfo("en-US").NumberFormat;
                }
                return _numberFormat;
            }
        }
        static NumberFormatInfo _numberFormat;
        public static CultureInfo CultureInfo
        {
            get
            {
                if (_culture == null)
                {
                    _culture = new CultureInfo("en-US");
                }
                return _culture;
            }
        }
        static CultureInfo _culture;
    }
}
